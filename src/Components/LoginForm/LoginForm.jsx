import './LoginForm.css';

function LoginForm(props){
    return <div >
        <br/>
        <form className='form-login'>
            <span className='title'>Login to account</span>
            <div className='input-field'>
                <input type='text' id='username1' placeholder='Enter your login' required/>
            </div>
            <div className='input-field'>
                <input type='password' id='password1' placeholder='Enter your password' required/>
            </div>
            <div className='input-field button'>
                <input type='button' id='login1' value='Login' onClick={props.onLoginClick}/>
            </div>
        </form>
    </div>
}

LoginForm.defaultProps = {onLoginClick :{}}
export default LoginForm;