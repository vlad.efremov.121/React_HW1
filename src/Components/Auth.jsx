import LoginForm from './LoginForm/LoginForm';
import LogoutForm from './LogoutForm/LogoutForm';
import Login from '../Api/Login/LoginApi';
import { useState } from 'react';

function Auth(){
    const [isLogin, setIsLogin] = useState(false);
    const [userName, setUserName] = useState('');

    async function loginHandler(){
        const userNameValue = document.getElementById('username1').value;
        const passwordValue = document.getElementById('password1').value;

        try{
            const result = await Login(userNameValue,passwordValue);
            if(result.status == 200){
                setIsLogin(true);
                setUserName(result.userName)
            }
            else
                alert(`Error status: ${result.status}`) 
        }catch(err){
            alert(`Error: ${err.message}`)
        }
    }

    function logoutHandler(){
        setIsLogin(false);
        setUserName(null);
    }
    
    if(!isLogin)
        return <div>
            <LoginForm onLoginClick={loginHandler}/>
        </div>
    else
        return <div>
            <LogoutForm userName={userName} onLogoutClick={logoutHandler}/>
        </div>
}

export default Auth;
