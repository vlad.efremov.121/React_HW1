function Menu(props){
    return  <header className="App-header">
        <h1 text-align="center">{props.title}</h1>
    </header>
}

Menu.defaultProps = {title:'Otus React App'};
export default Menu;