import './App.css';
import Auth from'./Components/Auth';
import Menu from'./Components/Menu';

function App() {
  return (
    <div className="App">
      <div className="menu">
        <Menu/>
      </div>
      <div className="auth">
        <Auth/>
      </div>
    </div>
  );
}

export default App;